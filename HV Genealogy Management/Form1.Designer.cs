﻿namespace HV_Genealogy_Management
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lstMember = new System.Windows.Forms.DataGridView();
            this.rdOther = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbSearch = new System.Windows.Forms.PictureBox();
            this.rdAll = new System.Windows.Forms.RadioButton();
            this.rdFemale = new System.Windows.Forms.RadioButton();
            this.rdMale = new System.Windows.Forms.RadioButton();
            this.txtSName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label6 = new System.Windows.Forms.Label();
            this.cbGenealogy = new System.Windows.Forms.ComboBox();
            this.lstREmember = new System.Windows.Forms.DataGridView();
            this.pbAvatar = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtAddress = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtState = new System.Windows.Forms.Label();
            this.txtJob = new System.Windows.Forms.Label();
            this.txtReligion = new System.Windows.Forms.Label();
            this.txtNationality = new System.Windows.Forms.Label();
            this.txtDOB = new System.Windows.Forms.Label();
            this.txtOtherName = new System.Windows.Forms.Label();
            this.txtSex = new System.Windows.Forms.Label();
            this.txtFullName = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.genealogyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAddNewGenealogy = new System.Windows.Forms.ToolStripMenuItem();
            this.btnUpdateGenealogy = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDeleteGenealogy = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShowDiagram = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShowStatistic = new System.Windows.Forms.ToolStripMenuItem();
            this.memberManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAddNewMember = new System.Windows.Forms.ToolStripMenuItem();
            this.btnUpdateMember = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDeleteMember = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.btnUserGuide = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstMember)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lstREmember)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAvatar)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.lstMember);
            this.groupBox2.Location = new System.Drawing.Point(3, 182);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(404, 202);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Member Information";
            // 
            // lstMember
            // 
            this.lstMember.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstMember.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lstMember.Location = new System.Drawing.Point(7, 20);
            this.lstMember.Name = "lstMember";
            this.lstMember.Size = new System.Drawing.Size(389, 176);
            this.lstMember.TabIndex = 0;
            this.lstMember.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.lstMember_CellClick);
            // 
            // rdOther
            // 
            this.rdOther.AutoSize = true;
            this.rdOther.Location = new System.Drawing.Point(331, 78);
            this.rdOther.Name = "rdOther";
            this.rdOther.Size = new System.Drawing.Size(51, 17);
            this.rdOther.TabIndex = 11;
            this.rdOther.Text = "Other";
            this.rdOther.UseVisualStyleBackColor = true;
            this.rdOther.CheckedChanged += new System.EventHandler(this.rdOther_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.rdOther);
            this.groupBox1.Controls.Add(this.pbSearch);
            this.groupBox1.Controls.Add(this.rdAll);
            this.groupBox1.Controls.Add(this.rdFemale);
            this.groupBox1.Controls.Add(this.rdMale);
            this.groupBox1.Controls.Add(this.txtSName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 135);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Quick search";
            // 
            // pbSearch
            // 
            this.pbSearch.Image = global::HV_Genealogy_Management.Properties.Resources.Logo;
            this.pbSearch.Location = new System.Drawing.Point(6, 24);
            this.pbSearch.Name = "pbSearch";
            this.pbSearch.Size = new System.Drawing.Size(100, 97);
            this.pbSearch.TabIndex = 10;
            this.pbSearch.TabStop = false;
            // 
            // rdAll
            // 
            this.rdAll.AutoSize = true;
            this.rdAll.Checked = true;
            this.rdAll.Location = new System.Drawing.Point(162, 78);
            this.rdAll.Name = "rdAll";
            this.rdAll.Size = new System.Drawing.Size(36, 17);
            this.rdAll.TabIndex = 5;
            this.rdAll.TabStop = true;
            this.rdAll.Text = "All";
            this.rdAll.UseVisualStyleBackColor = true;
            this.rdAll.CheckedChanged += new System.EventHandler(this.rdAll_CheckedChanged);
            // 
            // rdFemale
            // 
            this.rdFemale.AutoSize = true;
            this.rdFemale.Location = new System.Drawing.Point(264, 78);
            this.rdFemale.Name = "rdFemale";
            this.rdFemale.Size = new System.Drawing.Size(59, 17);
            this.rdFemale.TabIndex = 4;
            this.rdFemale.Text = "Female";
            this.rdFemale.UseVisualStyleBackColor = true;
            this.rdFemale.CheckedChanged += new System.EventHandler(this.rdFemale_CheckedChanged);
            // 
            // rdMale
            // 
            this.rdMale.AutoSize = true;
            this.rdMale.Location = new System.Drawing.Point(207, 78);
            this.rdMale.Name = "rdMale";
            this.rdMale.Size = new System.Drawing.Size(48, 17);
            this.rdMale.TabIndex = 3;
            this.rdMale.Text = "Male";
            this.rdMale.UseVisualStyleBackColor = true;
            this.rdMale.CheckedChanged += new System.EventHandler(this.rdMale_CheckedChanged);
            // 
            // txtSName
            // 
            this.txtSName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSName.Location = new System.Drawing.Point(162, 28);
            this.txtSName.Name = "txtSName";
            this.txtSName.Size = new System.Drawing.Size(220, 20);
            this.txtSName.TabIndex = 2;
            this.txtSName.TextChanged += new System.EventHandler(this.txtSName_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(112, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sex";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(110, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // splitContainer1
            // 
            this.splitContainer1.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Location = new System.Drawing.Point(5, 32);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(246)))), ((int)(((byte)(246)))));
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.cbGenealogy);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.lstREmember);
            this.splitContainer1.Panel2.Controls.Add(this.pbAvatar);
            this.splitContainer1.Panel2.Controls.Add(this.label5);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(1045, 395);
            this.splitContainer1.SplitterDistance = 423;
            this.splitContainer1.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 12);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "List of genealogy";
            // 
            // cbGenealogy
            // 
            this.cbGenealogy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGenealogy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbGenealogy.FormattingEnabled = true;
            this.cbGenealogy.Location = new System.Drawing.Point(116, 12);
            this.cbGenealogy.Name = "cbGenealogy";
            this.cbGenealogy.Size = new System.Drawing.Size(291, 21);
            this.cbGenealogy.TabIndex = 11;
            this.cbGenealogy.SelectionChangeCommitted += new System.EventHandler(this.cbGenealogy_SelectionChangeCommitted);
            // 
            // lstREmember
            // 
            this.lstREmember.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstREmember.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lstREmember.Location = new System.Drawing.Point(7, 204);
            this.lstREmember.Name = "lstREmember";
            this.lstREmember.Size = new System.Drawing.Size(592, 176);
            this.lstREmember.TabIndex = 12;
            // 
            // pbAvatar
            // 
            this.pbAvatar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbAvatar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbAvatar.Image = global::HV_Genealogy_Management.Properties.Resources.noneAvatar;
            this.pbAvatar.Location = new System.Drawing.Point(481, 31);
            this.pbAvatar.Name = "pbAvatar";
            this.pbAvatar.Size = new System.Drawing.Size(118, 123);
            this.pbAvatar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbAvatar.TabIndex = 11;
            this.pbAvatar.TabStop = false;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(517, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 19);
            this.label5.TabIndex = 11;
            this.label5.Text = "Avatar";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 19);
            this.label4.TabIndex = 11;
            this.label4.Text = "List of Relative";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(3, 9);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(472, 167);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtAddress);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtState);
            this.tabPage1.Controls.Add(this.txtJob);
            this.tabPage1.Controls.Add(this.txtReligion);
            this.tabPage1.Controls.Add(this.txtNationality);
            this.tabPage1.Controls.Add(this.txtDOB);
            this.tabPage1.Controls.Add(this.txtOtherName);
            this.tabPage1.Controls.Add(this.txtSex);
            this.tabPage1.Controls.Add(this.txtFullName);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(464, 141);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Personal Information";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtAddress
            // 
            this.txtAddress.AutoSize = true;
            this.txtAddress.Location = new System.Drawing.Point(226, 39);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(33, 13);
            this.txtAddress.TabIndex = 40;
            this.txtAddress.Text = "None";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(158, 39);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 13);
            this.label14.TabIndex = 39;
            this.label14.Text = "Address :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(326, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "State :";
            // 
            // txtState
            // 
            this.txtState.AutoSize = true;
            this.txtState.Location = new System.Drawing.Point(383, 102);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(33, 13);
            this.txtState.TabIndex = 37;
            this.txtState.Text = "None";
            // 
            // txtJob
            // 
            this.txtJob.AutoSize = true;
            this.txtJob.Location = new System.Drawing.Point(77, 102);
            this.txtJob.Name = "txtJob";
            this.txtJob.Size = new System.Drawing.Size(33, 13);
            this.txtJob.TabIndex = 36;
            this.txtJob.Text = "None";
            // 
            // txtReligion
            // 
            this.txtReligion.AutoSize = true;
            this.txtReligion.Location = new System.Drawing.Point(383, 70);
            this.txtReligion.Name = "txtReligion";
            this.txtReligion.Size = new System.Drawing.Size(33, 13);
            this.txtReligion.TabIndex = 35;
            this.txtReligion.Text = "None";
            // 
            // txtNationality
            // 
            this.txtNationality.AutoSize = true;
            this.txtNationality.Location = new System.Drawing.Point(226, 71);
            this.txtNationality.Name = "txtNationality";
            this.txtNationality.Size = new System.Drawing.Size(33, 13);
            this.txtNationality.TabIndex = 34;
            this.txtNationality.Text = "None";
            // 
            // txtDOB
            // 
            this.txtDOB.AutoSize = true;
            this.txtDOB.Location = new System.Drawing.Point(77, 71);
            this.txtDOB.Name = "txtDOB";
            this.txtDOB.Size = new System.Drawing.Size(33, 13);
            this.txtDOB.TabIndex = 33;
            this.txtDOB.Text = "None";
            // 
            // txtOtherName
            // 
            this.txtOtherName.AutoSize = true;
            this.txtOtherName.Location = new System.Drawing.Point(77, 38);
            this.txtOtherName.Name = "txtOtherName";
            this.txtOtherName.Size = new System.Drawing.Size(33, 13);
            this.txtOtherName.TabIndex = 32;
            this.txtOtherName.Text = "None";
            // 
            // txtSex
            // 
            this.txtSex.AutoSize = true;
            this.txtSex.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSex.Location = new System.Drawing.Point(383, 11);
            this.txtSex.Name = "txtSex";
            this.txtSex.Size = new System.Drawing.Size(41, 15);
            this.txtSex.TabIndex = 31;
            this.txtSex.Text = "None";
            // 
            // txtFullName
            // 
            this.txtFullName.AutoSize = true;
            this.txtFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFullName.Location = new System.Drawing.Point(77, 9);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(41, 15);
            this.txtFullName.TabIndex = 30;
            this.txtFullName.Text = "None";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 102);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Job :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(326, 71);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Religion :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(158, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 13);
            this.label11.TabIndex = 27;
            this.label11.Text = "Nationality :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "Date of birth :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Other name :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(326, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Sex :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Full Name :";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.genealogyToolStripMenuItem,
            this.memberManagerToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1058, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // genealogyToolStripMenuItem
            // 
            this.genealogyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddNewGenealogy,
            this.btnUpdateGenealogy,
            this.btnDeleteGenealogy,
            this.btnShowDiagram,
            this.btnShowStatistic});
            this.genealogyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("genealogyToolStripMenuItem.Image")));
            this.genealogyToolStripMenuItem.Name = "genealogyToolStripMenuItem";
            this.genealogyToolStripMenuItem.Size = new System.Drawing.Size(141, 20);
            this.genealogyToolStripMenuItem.Text = "Genealogy manager";
            // 
            // btnAddNewGenealogy
            // 
            this.btnAddNewGenealogy.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNewGenealogy.Image")));
            this.btnAddNewGenealogy.Name = "btnAddNewGenealogy";
            this.btnAddNewGenealogy.Size = new System.Drawing.Size(208, 22);
            this.btnAddNewGenealogy.Text = "Create new genealogy";
            this.btnAddNewGenealogy.Click += new System.EventHandler(this.btnAddNewGenealogy_Click);
            // 
            // btnUpdateGenealogy
            // 
            this.btnUpdateGenealogy.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateGenealogy.Image")));
            this.btnUpdateGenealogy.Name = "btnUpdateGenealogy";
            this.btnUpdateGenealogy.Size = new System.Drawing.Size(208, 22);
            this.btnUpdateGenealogy.Text = "Update Genealogy";
            this.btnUpdateGenealogy.Click += new System.EventHandler(this.btnUpdateGenealogy_Click);
            // 
            // btnDeleteGenealogy
            // 
            this.btnDeleteGenealogy.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteGenealogy.Image")));
            this.btnDeleteGenealogy.Name = "btnDeleteGenealogy";
            this.btnDeleteGenealogy.Size = new System.Drawing.Size(208, 22);
            this.btnDeleteGenealogy.Text = "Delete genealogy";
            this.btnDeleteGenealogy.Click += new System.EventHandler(this.btnDeleteGenealogy_Click);
            // 
            // btnShowDiagram
            // 
            this.btnShowDiagram.Image = ((System.Drawing.Image)(resources.GetObject("btnShowDiagram.Image")));
            this.btnShowDiagram.Name = "btnShowDiagram";
            this.btnShowDiagram.Size = new System.Drawing.Size(208, 22);
            this.btnShowDiagram.Text = "Show genealogy diagram";
            this.btnShowDiagram.Click += new System.EventHandler(this.btnShowDiagram_Click);
            // 
            // btnShowStatistic
            // 
            this.btnShowStatistic.Image = ((System.Drawing.Image)(resources.GetObject("btnShowStatistic.Image")));
            this.btnShowStatistic.Name = "btnShowStatistic";
            this.btnShowStatistic.Size = new System.Drawing.Size(208, 22);
            this.btnShowStatistic.Text = "Show statistic";
            this.btnShowStatistic.Click += new System.EventHandler(this.btnShowStatistic_Click);
            // 
            // memberManagerToolStripMenuItem
            // 
            this.memberManagerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddNewMember,
            this.btnUpdateMember,
            this.btnDeleteMember});
            this.memberManagerToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("memberManagerToolStripMenuItem.Image")));
            this.memberManagerToolStripMenuItem.Name = "memberManagerToolStripMenuItem";
            this.memberManagerToolStripMenuItem.Size = new System.Drawing.Size(130, 20);
            this.memberManagerToolStripMenuItem.Text = "Member manager";
            // 
            // btnAddNewMember
            // 
            this.btnAddNewMember.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNewMember.Image")));
            this.btnAddNewMember.Name = "btnAddNewMember";
            this.btnAddNewMember.Size = new System.Drawing.Size(169, 22);
            this.btnAddNewMember.Text = "Add new member";
            this.btnAddNewMember.Click += new System.EventHandler(this.btnAddNewMember_Click);
            // 
            // btnUpdateMember
            // 
            this.btnUpdateMember.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateMember.Image")));
            this.btnUpdateMember.Name = "btnUpdateMember";
            this.btnUpdateMember.Size = new System.Drawing.Size(169, 22);
            this.btnUpdateMember.Text = "Update Member";
            this.btnUpdateMember.Click += new System.EventHandler(this.btnUpdateMember_Click);
            // 
            // btnDeleteMember
            // 
            this.btnDeleteMember.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteMember.Image")));
            this.btnDeleteMember.Name = "btnDeleteMember";
            this.btnDeleteMember.Size = new System.Drawing.Size(169, 22);
            this.btnDeleteMember.Text = "Delete member";
            this.btnDeleteMember.Click += new System.EventHandler(this.btnDeleteMember_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnUserGuide,
            this.btnAbout});
            this.aboutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("aboutToolStripMenuItem.Image")));
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // btnAbout
            // 
            this.btnAbout.Image = ((System.Drawing.Image)(resources.GetObject("btnAbout.Image")));
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(152, 22);
            this.btnAbout.Text = "About HVGM";
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnUserGuide
            // 
            this.btnUserGuide.Image = ((System.Drawing.Image)(resources.GetObject("btnUserGuide.Image")));
            this.btnUserGuide.Name = "btnUserGuide";
            this.btnUserGuide.Size = new System.Drawing.Size(152, 22);
            this.btnUserGuide.Text = "User guide";
            this.btnUserGuide.Click += new System.EventHandler(this.btnUserGuide_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 439);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HV GENEALOGY MANAGEMENT";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstMember)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSearch)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lstREmember)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAvatar)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView lstMember;
        private System.Windows.Forms.RadioButton rdOther;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pbSearch;
        private System.Windows.Forms.RadioButton rdAll;
        private System.Windows.Forms.RadioButton rdFemale;
        private System.Windows.Forms.RadioButton rdMale;
        private System.Windows.Forms.TextBox txtSName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbGenealogy;
        private System.Windows.Forms.PictureBox pbAvatar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripMenuItem btnAbout;
        private System.Windows.Forms.ToolStripMenuItem btnAddNewGenealogy;
        private System.Windows.Forms.ToolStripMenuItem btnUpdateGenealogy;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnDeleteMember;
        private System.Windows.Forms.ToolStripMenuItem btnAddNewMember;
        private System.Windows.Forms.ToolStripMenuItem btnUpdateMember;
        private System.Windows.Forms.ToolStripMenuItem memberManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem genealogyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnShowDiagram;
        private System.Windows.Forms.ToolStripMenuItem btnDeleteGenealogy;
        private System.Windows.Forms.ToolStripMenuItem btnShowStatistic;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label txtFullName;
        private System.Windows.Forms.Label txtState;
        private System.Windows.Forms.Label txtJob;
        private System.Windows.Forms.Label txtReligion;
        private System.Windows.Forms.Label txtNationality;
        private System.Windows.Forms.Label txtDOB;
        private System.Windows.Forms.Label txtOtherName;
        private System.Windows.Forms.Label txtSex;
        private System.Windows.Forms.DataGridView lstREmember;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label txtAddress;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStripMenuItem btnUserGuide;
    }
}

