﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;

namespace HV_Genealogy_Management
{
    public partial class Add_new_Member : Form
    {
        private bool checkSelectRelationshipTab = false;
        private FamilyDatabase db = new FamilyDatabase();
        private int paID;
        private Form1 form1;
        private Image dImg = null;

        public Add_new_Member(Form1 _form, int id)
        {
            InitializeComponent();
            paID = id;
            form1 = _form;
        }

        private bool CheckParentage()
        {
            if (db.Members.Where(m => m.Parentage_ID == paID).ToList().Count > 0)
                return true;
            else
                return false;
                
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtFirstName.Text.Length == 0 || txtLastName.Text.Length == 0)
                {
                    MessageBox.Show("You must fill in all of the fields !", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (CheckParentage() == true)
                {
                    if ((cbFather.Text.Equals("None") && cbMother.Text.Equals("None") && cbSponse.Text.Equals("None")) || checkSelectRelationshipTab == false)
                    {
                        MessageBox.Show("You must set the relationship !", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                Member mem = new Member();
                int tempID = CreateID();
                mem.Member_ID = tempID;
                mem.Last_Name = txtLastName.Text;
                mem.First_Name = txtFirstName.Text;
                mem.Middle_Name = txtMiddleName.Text;
                mem.DOB = dtpDOB.Value;
                mem.Nationality = cbNationality.Text;
                mem.Religion = cbReligion.Text;
                mem.Job_Name = txtJob.Text;
                mem.Address = txtAddress.Text;
                mem.Parentage_ID = paID;
                mem.avatar = imageToByteArray(pbAvatar.Image);


                /* Check relationship */



                if (CheckParentage() == true)
                {
                    if ((cbFather.Text.Equals(cbMother.Text) && !cbFather.Text.Equals("None") && !cbMother.Text.Equals("None")) || (cbFather.Text.Equals(cbSponse.Text) && !cbFather.Text.Equals("None") && !cbSponse.Text.Equals("None")) || (cbMother.Text.Equals(cbSponse.Text) && !cbMother.Text.Equals("None") && !cbSponse.Text.Equals("None")))
                    {
                        MessageBox.Show("Duplicate relatioship !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    /* CHECK 3 MEMBERS */
                    if (Check3members(cbFather.Text, cbMother.Text, cbSponse.Text) == false)
                    {
                        MessageBox.Show("Error relatioship, please check !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    /* SAVE THE VALUE ID TO MEMBER */
                    if (!cbFather.Text.Equals("None"))
                    {
                        Member father = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(cbFather.Text) && (p.Parentage_ID == paID));
                        mem.Dad_ID = father.Member_ID;
                    }

                    if (!cbMother.Text.Equals("None"))
                    {
                        Member mother = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(cbMother.Text) && (p.Parentage_ID == paID));
                        mem.Mom_ID = mother.Member_ID;
                    }

                    if (!cbSponse.Text.Equals("None"))
                    {
                        Member spouse = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(cbSponse.Text) && (p.Parentage_ID == paID));
                        mem.Spouse_ID = spouse.Member_ID;
                        spouse.Spouse_ID = tempID;
                    }
                }


                if (rdFemale.Checked)
                    mem.Sex = "Female";
                if (rdMale.Checked)
                    mem.Sex = "Male";
                if (rdOther.Checked)
                    mem.Sex = "Other";

                if (ckDead.Checked)
                    mem.isDead = 1;
                else
                    mem.isDead = 0;

                /* set member to Chieftain */
                if (CheckParentage() == false)
                {
                    Parentage pa = db.Parentages.Single(p => p.Parentage_ID == paID);
                    pa.Chieftain_ID = tempID;
                }

                db.Members.AddObject(mem);
                db.SaveChanges();
                MessageBox.Show("Member has been added !", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetControlToDefault();
                Add_new_Member_Load(this, EventArgs.Empty);
                form1.Form1_Load(form1, EventArgs.Empty);
                
            }
            catch
            {
                MessageBox.Show("Failed, try again !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private bool Check3members(String name0, String name1, String name2)
        {
            if (name0.Equals("None") && name1.Equals("None") && name2.Equals("None"))
                return true;

            // Father and mother
            if (!name0.Equals("None") && !name1.Equals("None") && name2.Equals("None"))
            {
                if (CheckFatherAndMother(name0, name1))
                    return true;
                else
                    return false;
                
            }

            // Father and spouse
            if (!name0.Equals("None") && name1.Equals("None") && !name2.Equals("None"))
            {
                if (CheckFatherAndSpouse(name0, name2))
                    return true;
                else
                    return false;
            }

            // Mother and spouse
            if (name0.Equals("None") && !name1.Equals("None") && !name2.Equals("None"))
            {
                if (CheckMotherAndSpouse(name1, name2))
                    return true;
                else
                    return false;
            }

            if (!name0.Equals("None") && !name1.Equals("None") && !name2.Equals("None"))
                if (CheckFatherAndSpouse(name0, name2) && CheckMotherAndSpouse(name1, name2) && CheckFatherAndMother(name0, name1))
                    return true;
                else
                    return false;

            return true;
    
        }

        private bool CheckMotherAndSpouse(String name0, String name1)
        {
            Member mother = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name0) && (p.Parentage_ID == paID));
            Member spouse = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name1) && (p.Parentage_ID == paID));

            if ((mother.Member_ID == spouse.Mom_ID) || (spouse.Member_ID == mother.Mom_ID) || (spouse.Member_ID == mother.Dad_ID) || (mother.Dad_ID == spouse.Dad_ID && mother.Dad_ID != null && spouse.Dad_ID != null) || (mother.Mom_ID == spouse.Mom_ID && mother.Mom_ID != null && spouse.Mom_ID != null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckFatherAndSpouse(String name0, String name1)
        {
            Member father = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name0) && (p.Parentage_ID == paID));
            Member spouse = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name1) && (p.Parentage_ID == paID));

            if ((father.Member_ID == spouse.Dad_ID) || (spouse.Member_ID == father.Mom_ID) || (spouse.Member_ID == father.Dad_ID) || (father.Dad_ID == spouse.Dad_ID && father.Dad_ID != null && spouse.Dad_ID != null) || (father.Mom_ID == spouse.Mom_ID && father.Mom_ID != null && spouse.Mom_ID != null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckFatherAndMother(String name0, String name1)
        {
            Member father = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name0) && (p.Parentage_ID == paID));
            Member mother = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name1) && (p.Parentage_ID == paID));

            if ((father.Member_ID == mother.Dad_ID) || (mother.Member_ID == father.Mom_ID) || (father.Dad_ID == mother.Dad_ID && father.Dad_ID != null && mother.Dad_ID != null) || (father.Mom_ID == mother.Mom_ID && father.Mom_ID != null && mother.Mom_ID != null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void SetControlToDefault()
        {
            txtLastName.Text = "";
            txtFirstName.Text = "";
            txtMiddleName.Text = "";
            txtOtherName.Text = "";
            dtpDOB.Value = DateTime.UtcNow.Date;
            cbNationality.Text = "";
            cbReligion.Text = "";
            txtJob.Text = "";
            txtAddress.Text = "";
            ckDead.Checked = false;
            rdMale.Checked = true;
            if (dImg != null)
                pbAvatar.Image = dImg;
        }

        private void Add_new_Member_Load(object sender, EventArgs e)
        {
            cbReligion.SelectedIndex = 0;
            cbNationality.SelectedIndex = 0;
            dtpDOB.Value = DateTime.UtcNow.Date;

            var fatherList =
                from m in db.Members
                where ((m.Sex.Equals("Male") || m.Sex.Equals("Other")) && m.Parentage_ID == paID)
                select new
                {
                    Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                };

             var motherList =
                from m in db.Members
                where ((m.Sex.Equals("Female") || m.Sex.Equals("Other")) && m.Parentage_ID == paID)
                select new
                {
                    Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                };

             var spouseList =
                from m in db.Members
                where (m.Parentage_ID == paID && m.Spouse_ID == null)
                select new
                {
                    Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                };
             try
             {
                 DataTable fa = new DataTable();
                 fa.Columns.Add("Fullname");
                 DataRow data1 = fa.NewRow();
                 data1["Fullname"] = "None";
                 fa.Rows.Add(data1);
                 fa.Merge(ToDataTable(fatherList.ToList()));

                 DataTable mo = new DataTable();
                 mo.Columns.Add("Fullname");
                 DataRow data2 = mo.NewRow();
                 data2["Fullname"] = "None";
                 mo.Rows.Add(data2);
                 mo.Merge(ToDataTable(motherList.ToList()));

                 DataTable sp = new DataTable();
                 sp.Columns.Add("Fullname");
                 DataRow data3 = sp.NewRow();
                 data3["Fullname"] = "None";
                 sp.Rows.Add(data3);
                 sp.Merge(ToDataTable(spouseList.ToList()));

                 cbFather.DataSource = fa;
                 cbFather.DisplayMember = "Fullname";
                 cbMother.DataSource = mo;
                 cbMother.DisplayMember = "Fullname";
                 cbSponse.DataSource = sp;
                 cbSponse.DisplayMember = "Fullname";
             }
             catch
             {
             }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        private int CreateID()
        {
            return (DateTime.UtcNow.Second + DateTime.UtcNow.Minute * 60 + DateTime.UtcNow.Hour * 3600 + DateTime.UtcNow.Day + DateTime.UtcNow.Month + DateTime.UtcNow.Year);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pbAvatar_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog f = new OpenFileDialog();
                f.InitialDirectory = "C:/Picture/";
                f.Filter = "All Files|*.*|JPEGs|*.jpg|Bitmaps|*.bmp|GIFs|*.gif";
                f.FilterIndex = 2;
                if (f.ShowDialog() == DialogResult.OK)
                {
                    if (dImg == null)
                        dImg = pbAvatar.Image;

                    pbAvatar.Image = Image.FromFile(f.FileName);
                }
            }
            catch 
            { 
            }
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage2"])
            {
                checkSelectRelationshipTab = true;
            }
        }


    }
}
