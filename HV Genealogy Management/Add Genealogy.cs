﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HV_Genealogy_Management
{
    public partial class Add_Genealogy : Form
    {
        private FamilyDatabase db = new FamilyDatabase();

        private Form1 form1;

        public Add_Genealogy(Form1 _form1)
        {
            InitializeComponent();
            form1 = _form1;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (this.txtPN.Text == "" || this.txtCN.Text == "" || this.nNOL.Value == 0)
            {
                MessageBox.Show("You must fill in all of the fields !", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (CheckParentage(txtPN.Text))
            {
                MessageBox.Show("Parentage has been existed !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //try
            //{
                Parentage pa = new Parentage();
                pa.Parentage_ID = CreateID();
                pa.Parentage_Name = txtPN.Text;
                pa.Creator_Name = txtCN.Text;
                pa.Num_Life = Decimal.ToInt32(nNOL.Value);
                pa.Description = txtDes.Text;
                db.Parentages.AddObject(pa);
                db.SaveChanges();
                
                MessageBox.Show("Parentage has been added !", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetControlToDefault();
                Properties.Settings.Default.GenealogyValue = db.Parentages.ToList().Count - 1;
                Properties.Settings.Default.Save();
                form1.Form1_Load(form1, EventArgs.Empty);
            //}
            //catch
            //{
              //  MessageBox.Show("Please try again !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }

        private int CreateID()
        {
            return (DateTime.UtcNow.Second + DateTime.UtcNow.Minute*60 + DateTime.UtcNow.Hour*3600 + DateTime.UtcNow.Day + DateTime.UtcNow.Month + DateTime.UtcNow.Year);
        }

        private bool CheckParentage(String name)
        {
            bool flag = false;
            if (db.Parentages.Where(p => p.Parentage_Name.Equals(txtPN.Text.Trim())).ToList().Count > 0)
                flag = true;

            return flag;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetControlToDefault()
        {
            txtCN.Text = "";
            txtDes.Text = "";
            txtPN.Text = "";
            nNOL.Value = 0;
        }

        private void Add_Genealogy_Load(object sender, EventArgs e)
        {

        }
    }
}
