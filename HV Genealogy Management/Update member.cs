﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace HV_Genealogy_Management
{
    public partial class Update_member : Form
    {
        private Form1 form1;
        private int memID, paID;
        private bool checkSelectRelationshipTab = false;
        private Member mem = null, cSpouse = null;
        private FamilyDatabase db = new FamilyDatabase();
        public Update_member(Form1 _form, int ID, int pID)
        {
            form1 = _form;
            InitializeComponent();
            memID = ID;
            paID = pID;
        }

        private void Update_member_Load(object sender, EventArgs e)
        {
            try
            {
                mem = db.Members.Single(m => m.Member_ID == memID);


                if (mem.Sex.Equals("Male"))
                    rdMale.Checked = true;
                if (mem.Sex.Equals("Female"))
                    rdFemale.Checked = true;
                if (mem.Sex.Equals("Other"))
                    rdOther.Checked = true;

                txtFirstName.Text = mem.First_Name;
                txtMiddleName.Text = mem.Middle_Name;
                txtLastName.Text = mem.Last_Name;
                txtOtherName.Text = mem.Other_Name;
                dtpDOB.Value = (DateTime)mem.DOB;
                txtJob.Text = mem.Job_Name;
                txtAddress.Text = mem.Address;
                if (mem.avatar != null)
                    pbAvatar.Image = byteArrayToImage(mem.avatar);


                for (int i = 0; i < cbNationality.Items.Count; i++)
                {
                    if (mem.Nationality.Equals(cbNationality.Items[i].ToString()))
                        cbNationality.SelectedIndex = i;
                }

                for (int i = 0; i < cbReligion.Items.Count; i++)
                {
                    if (mem.Religion.Equals(cbReligion.Items[i].ToString()))
                        cbReligion.SelectedIndex = i;
                }

                if (mem.isDead == 1)
                    ckDead.Checked = true;

                int pID = (int)mem.Parentage_ID;
                if (db.Parentages.Single(p => p.Parentage_ID == pID).Chieftain_ID == mem.Member_ID)
                {
                    cbFather.Enabled = false;
                    cbMother.Enabled = false;
                }

                /* RELATIONSHIP */
                var fatherList =
                from m in db.Members
                where ((m.Sex.Equals("Male") || m.Sex.Equals("Other")) && m.Parentage_ID == mem.Parentage_ID && m.Member_ID != mem.Member_ID)
                select new
                {
                    Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                };

                var motherList =
                   from m in db.Members
                   where ((m.Sex.Equals("Female") || m.Sex.Equals("Other")) && m.Parentage_ID == mem.Parentage_ID && m.Member_ID != mem.Member_ID)
                   select new
                   {
                       Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                   };

                var spouseList =
                   from m in db.Members
                   where (m.Parentage_ID == mem.Parentage_ID && (m.Member_ID == mem.Spouse_ID || m.Spouse_ID == null) && m.Member_ID != mem.Member_ID)
                   select new
                   {
                       Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                   };

                try
                {
                    DataTable fa = new DataTable();
                    fa.Columns.Add("Fullname");
                    DataRow data1 = fa.NewRow();
                    data1["Fullname"] = "None";
                    fa.Rows.Add(data1);
                    fa.Merge(ToDataTable(fatherList.ToList()));

                    DataTable mo = new DataTable();
                    mo.Columns.Add("Fullname");
                    DataRow data2 = mo.NewRow();
                    data2["Fullname"] = "None";
                    mo.Rows.Add(data2);
                    mo.Merge(ToDataTable(motherList.ToList()));

                    DataTable sp = new DataTable();
                    sp.Columns.Add("Fullname");
                    DataRow data3 = sp.NewRow();
                    data3["Fullname"] = "None";
                    sp.Rows.Add(data3);
                    sp.Merge(ToDataTable(spouseList.ToList()));


                    cbFather.DataSource = fa;
                    cbFather.DisplayMember = "Fullname";
                    cbMother.DataSource = mo;
                    cbMother.DisplayMember = "Fullname";
                    cbSpouse.DataSource = sp;
                    cbSpouse.DisplayMember = "Fullname";

                    /* SELECT FATHER */
                    if (mem.Dad_ID != null)
                    {
                        Member father = db.Members.Single(m => m.Member_ID == mem.Dad_ID && (m.Parentage_ID == paID));

                        for (int i = 0; i < cbFather.Items.Count; i++)
                        {
                            DataRowView row = cbFather.Items[i] as DataRowView;

                            if (row != null)
                            {
                                string displayValue = row["Fullname"].ToString();
                                if ((father.Last_Name + " " + father.Middle_Name + " " + father.First_Name).Equals(displayValue))
                                    cbFather.SelectedIndex = i;
                            }
                        }
                    }


                    /* SELECT MOTHER */
                    if (mem.Mom_ID != null)
                    {
                        Member mother = db.Members.Single(m => m.Member_ID == mem.Mom_ID && (m.Parentage_ID == paID));

                        for (int i = 0; i < cbMother.Items.Count; i++)
                        {
                            DataRowView row = cbMother.Items[i] as DataRowView;

                            if (row != null)
                            {
                                string displayValue = row["Fullname"].ToString();
                                if ((mother.Last_Name + " " + mother.Middle_Name + " " + mother.First_Name).Equals(displayValue))
                                    cbMother.SelectedIndex = i;
                            }
                        }
                    }


                    /* SELECT SPOUSE */
                    if (mem.Spouse_ID != null)
                    {
                        Member spouse = db.Members.Single(m => m.Member_ID == mem.Spouse_ID && (m.Parentage_ID == paID));

                        for (int i = 0; i < cbSpouse.Items.Count; i++)
                        {
                            DataRowView row = cbSpouse.Items[i] as DataRowView;

                            if (row != null)
                            {
                                String displayValue = row["Fullname"].ToString();
                                String spouseName = spouse.Last_Name + " " + spouse.Middle_Name + " " + spouse.First_Name;
                                if (spouseName.Equals(displayValue))
                                {
                                    cbSpouse.SelectedIndex = i;
                                    /* GET CURRENT SPOUSE */
                                    cSpouse = db.Members.Single(c => (c.Last_Name + " " + c.Middle_Name + " " + c.First_Name).Equals(spouseName) && (c.Parentage_ID == paID));
                                }
                            }
                        }
                    }
                }
                catch
                {
                }
            }
            catch
            {
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                Member mem = db.Members.Single(m => m.Member_ID == memID);

                if (rdMale.Checked)
                    mem.Sex = "Male";
                if (rdFemale.Checked)
                    mem.Sex = "Female";
                if (rdOther.Checked)
                    mem.Sex = "Other";

                mem.First_Name = txtFirstName.Text;
                mem.Middle_Name = txtMiddleName.Text;
                mem.Last_Name = txtLastName.Text;
                mem.Other_Name = txtOtherName.Text;
                mem.DOB = dtpDOB.Value;
                mem.Nationality = cbNationality.Text;
                mem.Religion = cbReligion.Text;
                mem.Job_Name = txtJob.Text;
                mem.Address = txtAddress.Text;
                mem.avatar = imageToByteArray(pbAvatar.Image);

                if (ckDead.Checked)
                    mem.isDead = 1;

                if (checkSelectRelationshipTab == true)
                {
                    if ((cbFather.Text.Equals(cbMother.Text) && !cbFather.Text.Equals("None") && !cbMother.Text.Equals("None")) || (cbFather.Text.Equals(cbSpouse.Text) && !cbFather.Text.Equals("None") && !cbSpouse.Text.Equals("None")) || (cbMother.Text.Equals(cbSpouse.Text) && !cbMother.Text.Equals("None") && !cbSpouse.Text.Equals("None")))
                    {
                        MessageBox.Show("Duplicate relationship !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    /* CHECK 3 MEMBERS */
                    if (Check3members(cbFather.Text, cbMother.Text, cbSpouse.Text) == false)
                    {
                        MessageBox.Show("Error relationship, please check !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    /* SAVE THE VALUE ID TO MEMBER */
                    if (!cbFather.Text.Equals("None"))
                    {
                        Member father = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(cbFather.Text) && (p.Parentage_ID == paID));
                        mem.Dad_ID = father.Member_ID;
                    }
                    else
                    {
                        mem.Dad_ID = null;

                    }

                    if (!cbMother.Text.Equals("None"))
                    {
                        Member mother = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(cbMother.Text) && (p.Parentage_ID == paID));
                        mem.Mom_ID = mother.Member_ID;
                    }
                    else
                    {
                        mem.Mom_ID = null;
                    }

                    if (!cbSpouse.Text.Equals("None"))
                    {
                        if (cSpouse != null)
                        {
                            cSpouse.Spouse_ID = null;

                        }
                        Member spouse = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(cbSpouse.Text) && (p.Parentage_ID == paID));
                        mem.Spouse_ID = spouse.Member_ID;
                        spouse.Spouse_ID = mem.Member_ID;
                    }
                    else
                    {
                        mem.Spouse_ID = null;
                        mem.isWife = 0;
                        if (cSpouse != null)
                            cSpouse.Spouse_ID = null;
                    }
                }
                db.SaveChanges();
                MessageBox.Show("Update successfully !", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                form1.Form1_Load(form1, EventArgs.Empty);
            }
            catch
            {
                MessageBox.Show("Failed, please try again !");
            }

        }

        private bool Check3members(String name0, String name1, String name2)
        {
            if (name0.Equals("None") && name1.Equals("None") && name2.Equals("None"))
                return true;

            // Father and mother
            if (!name0.Equals("None") && !name1.Equals("None") && name2.Equals("None"))
            {
                if (CheckFatherAndMother(name0, name1))
                    return true;
                else
                    return false;

            }

            // Father and spouse
            if (!name0.Equals("None") && name1.Equals("None") && !name2.Equals("None"))
            {
                if (CheckFatherAndSpouse(name0, name2))
                    return true;
                else
                    return false;
            }

            // Mother and spouse
            if (name0.Equals("None") && !name1.Equals("None") && !name2.Equals("None"))
            {
                if (CheckMotherAndSpouse(name1, name2))
                    return true;
                else
                    return false;
            }

            if (!name0.Equals("None") && !name1.Equals("None") && !name2.Equals("None"))
                if (CheckFatherAndSpouse(name0, name2) && CheckMotherAndSpouse(name1, name2) && CheckFatherAndMother(name0, name1))
                    return true;
                else
                    return false;

            return true;

        }

        private bool CheckMotherAndSpouse(String name0, String name1)
        {
            Member mother = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name0) && (p.Parentage_ID == paID));
            Member spouse = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name1) && (p.Parentage_ID == paID));

            if ((mother.Member_ID == spouse.Mom_ID) || (spouse.Member_ID == mother.Mom_ID) || (spouse.Member_ID == mother.Dad_ID) || (mother.Dad_ID == spouse.Dad_ID && mother.Dad_ID != null && spouse.Dad_ID != null) || (mother.Mom_ID == spouse.Mom_ID && mother.Mom_ID != null && spouse.Mom_ID != null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckFatherAndSpouse(String name0, String name1)
        {
            Member father = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name0) && (p.Parentage_ID == paID));
            Member spouse = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name1) && (p.Parentage_ID == paID));

            if ((father.Member_ID == spouse.Dad_ID) || (spouse.Member_ID == father.Mom_ID) || (spouse.Member_ID == father.Dad_ID) || (father.Dad_ID == spouse.Dad_ID && father.Dad_ID != null && spouse.Dad_ID != null) || (father.Mom_ID == spouse.Mom_ID && father.Mom_ID != null && spouse.Mom_ID != null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool CheckFatherAndMother(String name0, String name1)
        {
            Member father = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name0) && (p.Parentage_ID == paID));
            Member mother = db.Members.Single(p => (p.Last_Name + " " + p.Middle_Name + " " + p.First_Name).Equals(name1) && (p.Parentage_ID == paID));

            if ((father.Member_ID == mother.Dad_ID) || (mother.Member_ID == father.Mom_ID) || (father.Dad_ID == mother.Dad_ID && father.Dad_ID != null && mother.Dad_ID != null) || (father.Mom_ID == mother.Mom_ID && father.Mom_ID != null && mother.Mom_ID != null))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pbAvatar_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog f = new OpenFileDialog();
                f.InitialDirectory = "C:/Picture/";
                f.Filter = "All Files|*.*|JPEGs|*.jpg|Bitmaps|*.bmp|GIFs|*.gif";
                f.FilterIndex = 2;
                if (f.ShowDialog() == DialogResult.OK)
                {
                    pbAvatar.Image = Image.FromFile(f.FileName);
                }
            }
            catch
            {
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage2"])
            {
                checkSelectRelationshipTab = true;
            }
        }
    }
}
