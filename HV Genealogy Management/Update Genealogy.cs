﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TreeGenerator;

namespace HV_Genealogy_Management
{
    public partial class Update_Genealogy : Form
    {
        private FamilyDatabase db = new FamilyDatabase();

        private Form1 form1;

        private int pid;

        public Update_Genealogy(Form1 _form, int paID)
        {
            InitializeComponent();
            pid = paID;
            form1 = _form;
        }

        private void Update_Genealogy_Load(object sender, EventArgs e)
        {
            Parentage pa = db.Parentages.Single(p => p.Parentage_ID == pid);
            txtPN.Text = pa.Parentage_Name;
            txtCN.Text = pa.Creator_Name;
            nNOL.Value = pa.Num_Life;
            txtDes.Text = pa.Description;

            try
            {
                Member mem = db.Members.Single(m => m.Member_ID == pa.Chieftain_ID);
                txtChieftain.Text = mem.Last_Name + " " + mem.Middle_Name + " " + mem.First_Name;
            }
            catch
            {
                return;
            }
            
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (this.txtPN.Text == "" || this.txtCN.Text == "" || this.nNOL.Value == 0)
            {
                MessageBox.Show("You must fill in all of the fields !", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            try
            {
                Parentage pa = db.Parentages.Single(p => p.Parentage_ID == pid);
                pa.Parentage_ID = pid;
                pa.Parentage_Name = txtPN.Text;
                pa.Creator_Name = txtCN.Text;
                pa.Num_Life = Decimal.ToInt32(nNOL.Value);
                pa.Description = txtDes.Text;
                db.SaveChanges();

                MessageBox.Show("Parentage has been updated !", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                form1.Form1_Load(form1, EventArgs.Empty);
            }
            catch
            {
                MessageBox.Show("Please try again !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
