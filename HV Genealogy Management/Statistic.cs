﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HV_Genealogy_Management
{
    public partial class Statistic : Form
    {
        private int pID;
        private FamilyDatabase db = new FamilyDatabase();
        public Statistic(int paID)
        {
            InitializeComponent();
            pID = paID;
        }

        private void Statistic_Load(object sender, EventArgs e)
        {
            Parentage pa = db.Parentages.Single(p => p.Parentage_ID == pID);
            
            /* Parentage Name */
            ListViewItem p1 = new ListViewItem();
            p1.Text = "Parentage Name";
            p1.SubItems.Add(pa.Parentage_Name);
            lstStatistic.Items.Add(p1);

            /* Number of Life */
            ListViewItem p2 = new ListViewItem();
            p2.Text = "Number of Life";
            p2.SubItems.Add(pa.Num_Life.ToString());
            lstStatistic.Items.Add(p2);

            /* Total members */
            ListViewItem p3 = new ListViewItem();
            int tMember = db.Members.Where(m => m.Parentage_ID == pID).ToList().Count;
            p3.Text = "Total members";
            p3.SubItems.Add(tMember.ToString());
            lstStatistic.Items.Add(p3);

            /* Total wonmen */
            ListViewItem p4 = new ListViewItem();
            int tWonmen = db.Members.Where(m => m.Parentage_ID == pID && m.Sex.Equals("Female")).ToList().Count;
            p4.Text = "Total wonmen";
            p4.SubItems.Add(tWonmen.ToString());
            lstStatistic.Items.Add(p4);

            /* Total men */
            ListViewItem p5 = new ListViewItem();
            int tMen = db.Members.Where(m => m.Parentage_ID == pID && m.Sex.Equals("Male")).ToList().Count;
            p5.Text = "Total men";
            p5.SubItems.Add(tMen.ToString());
            lstStatistic.Items.Add(p5);

            /* Other */
            ListViewItem p6 = new ListViewItem();
            int tOther = db.Members.Where(m => m.Parentage_ID == pID && m.Sex.Equals("Other")).ToList().Count;
            p6.Text = "Other";
            p6.SubItems.Add(tOther.ToString());
            lstStatistic.Items.Add(p6);

            /* Age [0-5] */
            ListViewItem p7 = new ListViewItem();
            int tAge05 = db.Members.Where(m => m.Parentage_ID == pID && (DateTime.Now.Year - ((DateTime)m.DOB).Year) >= 0 && (DateTime.Now.Year - ((DateTime)m.DOB).Year) <= 5).ToList().Count;
            p7.Text = "Age [0-5]";
            p7.SubItems.Add(tAge05.ToString());
            lstStatistic.Items.Add(p7);

            /* Age [6-17] */
            ListViewItem p8 = new ListViewItem();
            int tAge617 = db.Members.Where(m => m.Parentage_ID == pID && (DateTime.Now.Year - ((DateTime)m.DOB).Year) >= 6 && (DateTime.Now.Year - ((DateTime)m.DOB).Year) <= 17).ToList().Count;
            p8.Text = "Age [6-17]";
            p8.SubItems.Add(tAge617.ToString());
            lstStatistic.Items.Add(p8);

            /* Age [18-35] */
            ListViewItem p9 = new ListViewItem();
            int tAge1835 = db.Members.Where(m => m.Parentage_ID == pID && (DateTime.Now.Year - ((DateTime)m.DOB).Year) >= 18 && (DateTime.Now.Year - ((DateTime)m.DOB).Year) <= 35).ToList().Count;
            p9.Text = "Age [18-35]";
            p9.SubItems.Add(tAge1835.ToString());
            lstStatistic.Items.Add(p9);

            /* Age [36-55] */
            ListViewItem p10 = new ListViewItem();
            int tAge3655 = db.Members.Where(m => m.Parentage_ID == pID && (DateTime.Now.Year - ((DateTime)m.DOB).Year) >= 36 && (DateTime.Now.Year - ((DateTime)m.DOB).Year) <= 55).ToList().Count;
            p10.Text = "Age [36-55]";
            p10.SubItems.Add(tAge3655.ToString());
            lstStatistic.Items.Add(p10);

            /* Age [56-70] */
            ListViewItem p11 = new ListViewItem();
            int tAge5670 = db.Members.Where(m => m.Parentage_ID == pID && (DateTime.Now.Year - ((DateTime)m.DOB).Year) >= 56 && (DateTime.Now.Year - ((DateTime)m.DOB).Year) <= 70).ToList().Count;
            p11.Text = "Age [56-70]";
            p11.SubItems.Add(tAge5670.ToString());
            lstStatistic.Items.Add(p11);

            /* Age [71 -> ] */
            ListViewItem p12 = new ListViewItem();
            int tAge71 = db.Members.Where(m => m.Parentage_ID == pID && (DateTime.Now.Year - ((DateTime)m.DOB).Year) >= 71).ToList().Count;
            p12.Text = "Age [Higher than 71]";
            p12.SubItems.Add(tAge71.ToString());
            lstStatistic.Items.Add(p12);
        }
    }
}
