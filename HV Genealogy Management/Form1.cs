﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Diagnostics;

namespace HV_Genealogy_Management
{
    public partial class Form1 : Form
    {
        private FamilyDatabase db = null;
        private Image dImg = Properties.Resources.noneAvatar;

        public Form1()
        {
            InitializeComponent();

        }

        private void btnAddNewGenealogy_Click(object sender, EventArgs e)
        {
            Add_Genealogy f = new Add_Genealogy(this);
            f.ShowDialog();

        }

        internal void Form1_Load(object sender, EventArgs e)
        {
            
            LoadParentage();
            
            if (cbGenealogy.Text.Length != 0)
            {
                Parentage pa  = db.Parentages.Single(ph => ph.Parentage_Name.Equals(cbGenealogy.Text));

                LoadListMember(pa.Parentage_ID);

            }

            UpdateStateOfMenu();
        }

        private void UpdateStateOfMenu()
        {
            if (this.cbGenealogy.Text.Length == 0)
            {
                /* set Menu disable*/
                this.btnUpdateGenealogy.Enabled = false;
                this.btnDeleteGenealogy.Enabled = false;
                this.btnDeleteGenealogy.Enabled = false;
                this.btnShowDiagram.Enabled = false;
                this.btnShowStatistic.Enabled = false;

                this.btnAddNewMember.Enabled = false;
                this.btnUpdateMember.Enabled = false;
                this.btnDeleteMember.Enabled = false;
            }
            else
            {
                Parentage pa = db.Parentages.Single(p => p.Parentage_Name.Equals(cbGenealogy.Text));
                int tpID = pa.Parentage_ID;

                this.btnUpdateGenealogy.Enabled = true;
                this.btnDeleteGenealogy.Enabled = true;
                this.btnDeleteGenealogy.Enabled = true;

                if (db.Members.Where(m => m.Parentage_ID == tpID).ToList().Count > 0)
                {
                    this.btnShowDiagram.Enabled = true;
                    this.btnShowStatistic.Enabled = true;
                    this.btnUpdateMember.Enabled = true;
                    this.btnDeleteMember.Enabled = true;
                }
                else
                {
                    this.btnShowDiagram.Enabled = false;
                    this.btnShowStatistic.Enabled = false;
                    this.btnUpdateMember.Enabled = false;
                    this.btnDeleteMember.Enabled = false;
                }

                this.btnAddNewMember.Enabled = true;
            }
        }

        private void LoadParentage()
        {
            db = new FamilyDatabase();
            cbGenealogy.DataSource = db.Parentages.Select(pa => new
            {
                Parentage_Name = pa.Parentage_Name
            }).ToList();

            cbGenealogy.DisplayMember = "Parentage_Name";
            if (db.Parentages.ToList().Count == 0)
                return;
            else
                cbGenealogy.SelectedIndex = Properties.Settings.Default.GenealogyValue;
        }

        private void btnUpdateGenealogy_Click(object sender, EventArgs e)
        {
            Parentage pa = db.Parentages.Single(p => p.Parentage_Name.Equals(cbGenealogy.Text));
            Update_Genealogy f = new Update_Genealogy(this, pa.Parentage_ID);
            f.ShowDialog();
        }

        private void btnDeleteGenealogy_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("All of members will be losted. Are you sure to delete this parentage ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.Yes)
            {
                Parentage pa = db.Parentages.Single(p => p.Parentage_Name.Equals(cbGenealogy.Text));
                deleteAllMembers(pa.Parentage_ID);
                db.Parentages.DeleteObject(pa);
                db.SaveChanges();
                if (db.Parentages.ToList().Count >= 1)
                {
                    if (cbGenealogy.SelectedIndex != 0)
                    {
                        Properties.Settings.Default.GenealogyValue = cbGenealogy.SelectedIndex - 1;
                    }
                }
                Form1_Load(null, null);
                lstMember.DataSource = null;
                lstREmember.DataSource = null;
                SetDefaultDisplay();
                MessageBox.Show("Delete successfully !", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                return;
            }
            /*
            try
            {
                
                       
            }
            catch
            {
                MessageBox.Show("Can't delete this parentage", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           */
        }

        private void deleteAllMembers(int ID)
        {
            
            List <Member> memList = db.Members.Where(m => m.Parentage_ID == ID).ToList();
            try
            {
                for (int i = 0; i < memList.Count; i++)
                {
                    int tid = memList[i].Member_ID;
                    Member mem = db.Members.Single(m => m.Member_ID == tid);
                    mem.Dad_ID = null;
                    mem.Mom_ID = null;
                    mem.Spouse_ID = null;
                    db.Members.DeleteObject(mem);
                    db.SaveChanges();
                }
            }
            catch
            {
            }
        }

        private void btnAddNewMember_Click(object sender, EventArgs e)
        {
            Parentage pa = db.Parentages.Single(p => p.Parentage_Name.Equals(cbGenealogy.Text));
            Add_new_Member f = new Add_new_Member(this, pa.Parentage_ID);
            f.ShowDialog();
        }

        private void cbGenealogy_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Properties.Settings.Default.GenealogyValue = cbGenealogy.SelectedIndex;
            Properties.Settings.Default.Save();
            Parentage pa = db.Parentages.Single(p => p.Parentage_Name.Equals(cbGenealogy.Text));
            LoadListMember(pa.Parentage_ID);
            UpdateStateOfMenu();
        }

        private void LoadListMember(int ID)
        {       
            db = new FamilyDatabase();
            var memList =
            from m in db.Members
            where m.Parentage_ID == ID
            select new
            {
                Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                Sex = m.Sex,
                DOB = m.DOB,
                OtherName = m.Other_Name,
                Nationality = m.Nationality,
                Religion = m.Religion,
                Parentage_ID = m.Parentage_ID,
                Member_ID = m.Member_ID
            };


            lstMember.DataSource = memList.ToList();

            lstMember.Columns[0].HeaderText = "Full name";
            lstMember.Columns[1].HeaderText = "Sex";
            lstMember.Columns[2].HeaderText = "Date of birth";
            lstMember.Columns[3].HeaderText = "Other name";
            lstMember.Columns[4].HeaderText = "Nationality";
            lstMember.Columns[5].HeaderText = "Religion";
            lstMember.Columns[6].Visible = false;
            lstMember.Columns[7].Visible = false;
        }

        private void LoadSearchList(int paID, String name)
        {
            string sex = "";
            

            if (rdAll.Checked == true)
            {
                var memList =
                from m in db.Members
                where (m.Parentage_ID == paID && (m.Last_Name.Contains(name) || m.First_Name.Contains(name) || m.Middle_Name.Contains(name)))
                select new
                {
                    Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                    Sex = m.Sex,
                    DOB = m.DOB,
                    OtherName = m.Other_Name,
                    Nationality = m.Nationality,
                    Religion = m.Religion,
                    Parentage_ID = m.Parentage_ID,
                    Member_ID = m.Member_ID
                };
                lstMember.DataSource = memList.ToList();
            }
            else
            {
                if (rdFemale.Checked)
                    sex = "Female";
                if (rdMale.Checked)
                    sex = "Male";
                if (rdOther.Checked)
                    sex = "Other";

                var memList =
                from m in db.Members
                where (m.Parentage_ID == paID && (m.Last_Name.Contains(name) || m.First_Name.Contains(name) || m.Middle_Name.Contains(name)) && m.Sex.Equals(sex))
                select new
                {
                    Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                    Sex = m.Sex,
                    DOB = m.DOB,
                    OtherName = m.Other_Name,
                    Nationality = m.Nationality,
                    Religion = m.Religion,
                    Parentage_ID = m.Parentage_ID,
                    Member_ID = m.Member_ID
                };
                lstMember.DataSource = memList.ToList();
            }

            lstMember.Columns[0].HeaderText = "Full name";
            lstMember.Columns[1].HeaderText = "Sex";
            lstMember.Columns[2].HeaderText = "Date of birth";
            lstMember.Columns[3].HeaderText = "Other name";
            lstMember.Columns[4].HeaderText = "Nationality";
            lstMember.Columns[5].HeaderText = "Religion";
            lstMember.Columns[6].Visible = false;
            lstMember.Columns[7].Visible = false;
        }

        private void btnUpdateMember_Click(object sender, EventArgs e)
        {
            int tID = (int) this.lstMember.CurrentRow.Cells[7].Value;
            Parentage pa = db.Parentages.Single(p => p.Parentage_Name.Equals(cbGenealogy.Text));
            int paID = pa.Parentage_ID;
            Update_member f = new Update_member(this, tID, paID);
            f.ShowDialog();
        
        }

        private void SetDefaultDisplay()
        {
            txtFullName.Text = "None";
            txtSex.Text = "None";
            txtOtherName.Text = "None";
            txtDOB.Text = "None";
            txtNationality.Text = "None";
            txtReligion.Text = "None";
            txtState.Text = "None";
            txtJob.Text = "None";
            txtAddress.Text = "None";
            pbAvatar.Image = dImg;

        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        private void lstMember_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                int tID = (int)this.lstMember.CurrentRow.Cells[7].Value;
                Member mem = db.Members.Single(m => m.Member_ID == tID);
                txtFullName.Text = (mem.Middle_Name != null) ? mem.Last_Name + " " + mem.Middle_Name + " " + mem.First_Name : mem.Last_Name + " " + mem.First_Name;
                txtOtherName.Text = (mem.Other_Name != null) ? mem.Other_Name : "None";
                txtSex.Text = mem.Sex;
                DateTime datetime = (DateTime)mem.DOB;
                txtDOB.Text = datetime.ToString("dd/MM/yyyy");
                txtNationality.Text = mem.Nationality;
                txtReligion.Text = mem.Religion;
                txtJob.Text = (mem.Job_Name.Length > 0) ? mem.Job_Name : "None";
                txtAddress.Text = (mem.Address.Length > 0) ? mem.Address : "None";
                txtState.Text = (mem.isDead == 1) ? "Dead" : "None";
                pbAvatar.Image = byteArrayToImage(mem.avatar);
                pbAvatar.SizeMode = PictureBoxSizeMode.StretchImage;
                pbAvatar.BorderStyle = BorderStyle.Fixed3D;

                loadRelatedList(mem);
            }
            catch
            {
            }
        }

        private void btnDeleteMember_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult = MessageBox.Show("Are you sure to delete this member ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                int tID = (int)this.lstMember.CurrentRow.Cells[7].Value;
                
                if (dialogResult == DialogResult.Yes)
                {
                    
                    Member mem = db.Members.Single(m => m.Member_ID == tID);
                    Parentage pa = db.Parentages.Single(p => p.Parentage_Name.Equals(cbGenealogy.Text));
                    
                    int mID = mem.Member_ID;
                    int pID = pa.Parentage_ID;

                    

                    if (pa.Chieftain_ID == tID && db.Members.Where(m => m.Parentage_ID == pID).ToList().Count > 1)
                    {
                        MessageBox.Show("This is the chieftain, you can't delete this member !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    else
                    {
                        UpdateForeignKey(pID, mID);
                        db.Members.DeleteObject(mem);
                        db.SaveChanges();
                        Form1_Load(null, null);
                        SetDefaultDisplay();
                        lstREmember.DataSource = null;
                        MessageBox.Show("Delete successfully !", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                }
                else
                {
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Can't delete this member!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void UpdateForeignKey(int pID, int id)
        {
            List<Member> listMem = db.Members.Where(m => m.Parentage_ID == pID).ToList();
            for (int i = 0; i < listMem.Count; i++)
            {
                int tID = listMem[i].Member_ID;
                Member mem = db.Members.Single(m => m.Member_ID == tID);
                if (id == mem.Dad_ID)
                {
                    mem.Dad_ID = null;
                    db.SaveChanges();
                }

                if (id == mem.Mom_ID)
                {
                    mem.Mom_ID = null;
                    db.SaveChanges();
                }

                if (id == mem.Spouse_ID)
                {
                    mem.Spouse_ID = null;
                    db.SaveChanges();
                }

            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        private void loadRelatedList(Member member)
        {
            String memberName = member.Last_Name + " " + member.Middle_Name + " " + member.First_Name;
            DataTable dataChild = new DataTable(), dataSpouse = new DataTable(), dataMom = new DataTable(), dataDad = new DataTable(), dataBro = new DataTable(), dataSis = new DataTable();

            var childrenList =
            from m in db.Members
            where (m.Dad_ID == member.Member_ID || m.Mom_ID == member.Member_ID)
            select new
            {
                Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                Sex = m.Sex,
                Relationship = "Children",
                DOB = m.DOB,
                Nationality = m.Nationality,
                Religion = m.Religion,
            };

            var spouseList =
            from m in db.Members
            where m.Member_ID == member.Spouse_ID
            select new
            {
                Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                Sex = m.Sex,
                Relationship = "Spouse",
                DOB = m.DOB,
                Nationality = m.Nationality,
                Religion = m.Religion,
            };

            var DadList =
            from m in db.Members
            where m.Member_ID == member.Dad_ID
            select new
            {
                Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                Sex = m.Sex,
                Relationship = "Father",
                DOB = m.DOB,
                Nationality = m.Nationality,
                Religion = m.Religion,
            };

            var MomList =
            from m in db.Members
            where m.Member_ID == member.Mom_ID
            select new
            {
                Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                Sex = m.Sex,
                Relationship = "Mother",
                DOB = m.DOB,
                Nationality = m.Nationality,
                Religion = m.Religion,
            };

            var BroList =
            from m in db.Members
            where ((m.Dad_ID == member.Dad_ID || m.Mom_ID == member.Mom_ID) && (m.Sex.Equals("Male") || m.Sex.Equals("Other")) && (!(m.Last_Name + " " + m.Middle_Name + " " + m.First_Name).Equals(memberName)))
            select new
            {
                Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                Sex = m.Sex,
                Relationship = "Brother",
                DOB = m.DOB,
                Nationality = m.Nationality,
                Religion = m.Religion,
            };

            var SisList =
            from m in db.Members
            where ((m.Dad_ID == member.Dad_ID || m.Mom_ID == member.Mom_ID) && (m.Sex.Equals("Female") || m.Sex.Equals("Other")) && (!(m.Last_Name + " " + m.Middle_Name + " " + m.First_Name).Equals(memberName)))
            select new
            {
                Fullname = m.Last_Name + " " + m.Middle_Name + " " + m.First_Name,
                Sex = m.Sex,
                Relationship = "Sister",
                DOB = m.DOB,
                Nationality = m.Nationality,
                Religion = m.Religion,
            };


            dataChild = (childrenList != null) ? ToDataTable(childrenList.ToList()) : null;
            dataDad = (DadList != null) ? ToDataTable(DadList.ToList()) : null;
            dataMom = (MomList != null) ? ToDataTable(MomList.ToList()) : null;
            dataSpouse = (spouseList != null) ? ToDataTable(spouseList.ToList()) : null;
            dataBro = (BroList != null) ? ToDataTable(BroList.ToList()) : null;
            dataSis = (SisList != null) ? ToDataTable(SisList.ToList()) : null;

            dataChild.Merge(dataDad);
            dataChild.Merge(dataMom);
            dataChild.Merge(dataSpouse);
            dataChild.Merge(dataBro);
            dataChild.Merge(dataSis);
            lstREmember.DataSource = dataChild;

            lstREmember.Columns[0].HeaderText = "Full name";
            lstREmember.Columns[1].HeaderText = "Relationship";
            lstREmember.Columns[2].HeaderText = "Sex";
            lstREmember.Columns[3].HeaderText = "Date of birth";
            lstREmember.Columns[4].HeaderText = "Nationality";
            lstREmember.Columns[5].HeaderText = "Religion";
            
        }

        private void txtSName_TextChanged(object sender, EventArgs e)
        {
            if (cbGenealogy.Text.Length == 0)
            {
                return;
            }
            else
            {
                Parentage pa = db.Parentages.Single(ph => ph.Parentage_Name.Equals(cbGenealogy.Text));
                int pID = pa.Parentage_ID;

                if (cbGenealogy.Text.Length != 0 && db.Members.Where(m => m.Parentage_ID == pID).ToList().Count > 0)
                {
                    if (txtSName.Text.Length != 0)
                    {
                        LoadSearchList(pa.Parentage_ID, txtSName.Text);
                    }
                    else
                    {
                        LoadListMember(pa.Parentage_ID);
                    }
                }
                else
                {
                    return;
                }
            }
        }

        private void rdAll_CheckedChanged(object sender, EventArgs e)
        {

            if (rdAll.Checked)
            {
                Parentage pa = db.Parentages.Single(ph => ph.Parentage_Name.Equals(cbGenealogy.Text));
                if (txtSName.Text.Length == 0)
                    LoadListMember(pa.Parentage_ID);
                else
                    LoadSearchList(pa.Parentage_ID, txtSName.Text);
            }
        }

        private void rdMale_CheckedChanged(object sender, EventArgs e)
        {
            Parentage pa = db.Parentages.Single(ph => ph.Parentage_Name.Equals(cbGenealogy.Text));
            if (txtSName.Text.Length != 0 && rdMale.Checked)
            {
                LoadSearchList(pa.Parentage_ID, txtSName.Text);
            }
        }

        private void rdFemale_CheckedChanged(object sender, EventArgs e)
        {
            Parentage pa = db.Parentages.Single(ph => ph.Parentage_Name.Equals(cbGenealogy.Text));
            if (txtSName.Text.Length != 0 && rdFemale.Checked)
            {
                LoadSearchList(pa.Parentage_ID, txtSName.Text);
            }
        }

        private void rdOther_CheckedChanged(object sender, EventArgs e)
        {
            Parentage pa = db.Parentages.Single(ph => ph.Parentage_Name.Equals(cbGenealogy.Text));
            if (txtSName.Text.Length != 0 && rdOther.Checked)
            {
                LoadSearchList(pa.Parentage_ID, txtSName.Text);
            }
        }

        private void btnShowStatistic_Click(object sender, EventArgs e)
        {
            Parentage pa = db.Parentages.Single(ph => ph.Parentage_Name.Equals(cbGenealogy.Text));
            Statistic f = new Statistic(pa.Parentage_ID);
            f.ShowDialog();
        }

        private void btnShowDiagram_Click(object sender, EventArgs e)
        {
            Parentage pa = db.Parentages.Single(ph => ph.Parentage_Name.Equals(cbGenealogy.Text));
            Genealogy_Diagram f = new Genealogy_Diagram(pa.Parentage_ID);
            f.ShowDialog();
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            About f = new About();
            f.ShowDialog();
        }

        private void btnUserGuide_Click(object sender, EventArgs e)
        {
            try
            {
                string pFile = AppDomain.CurrentDomain.BaseDirectory + "\\User guide.pdf";
                using (FileStream fileStream = new FileStream(@pFile, FileMode.Create, FileAccess.Write))
                {
                    using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
                    {
                        binaryWriter.Write(Properties.Resources.User_guide);
                    }
                }

                Process.Start(@pFile);
                return;
            }
            catch
            {
                MessageBox.Show("Undefined error, please try later !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

    }
}
