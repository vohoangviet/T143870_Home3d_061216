﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TreeGenerator;
using System.Drawing.Imaging;


namespace HV_Genealogy_Management
{
    public partial class Genealogy_Diagram : Form
    {
        private FamilyDatabase db = new FamilyDatabase();
        private TreeBuilder myTree = null;
        private int pID;

        public Genealogy_Diagram(int ID)
        {
            InitializeComponent();
            pID = ID;
        }

        private void Genealogy_Diagram_Load(object sender, EventArgs e)
        {

            

            /* Update STT of Members*/
            fillSTT();

            /* Find the chieftain */
            Parentage pa = db.Parentages.Single(p => p.Parentage_ID == pID);
            int cID = (int) pa.Chieftain_ID;

            /* Find and Update all of related members */
            FindAndUpdateRelatedMembers(cID);
            
            /* Set data and show tree */
            myTree = new TreeBuilder(GetTreeData());
            ShowTree();
           
        }
        

        private void FindAndUpdateRelatedMembers(int id)
        {
            foreach (Member a in db.Members)
            {
                int spouseID = FindAndUpdateSpouse(a.Member_ID);
                if (spouseID != -1)
                {
                    FindAndUpdateChild(spouseID);

                }
                else
                {
                    if (spouseID == -1)
                    {
                        int tid = a.Member_ID;
                        FindAndUpdateChild(tid);
                    }   
                }
            }
        }

        private int FindAndUpdateSpouse(int id)
        {
            int spouseID = -1;
            Member spouse = db.Members.Single(s => s.Member_ID == id);

            List<Member> mem = db.Members.ToList();


            for (int i = 0; i < mem.Count; i++)
            {
                int tid = mem[i].Member_ID;
                Member tmem = db.Members.Single(m => m.Member_ID == tid);

                if (spouse.Spouse_ID == tmem.Member_ID && spouse.Member_ID == tmem.Spouse_ID && tmem.Sex.Equals("Female"))
                {
                    tmem.isWife = 1;
                    tmem.num = spouse.stt;
                    spouseID = tmem.Member_ID;
                    db.SaveChanges();
                }

            }
            return spouseID;

        }

        private void FindAndUpdateChild(int id)
        {
            List<Member> mem = db.Members.ToList();

            for (int i = 0; i < mem.Count; i++)
            {
                int tid = mem[i].Member_ID;
                Member a = db.Members.Single(m => m.Member_ID == tid);

                if (a.Dad_ID != null && a.Mom_ID != null)
                {
                    int dadid = (int)a.Dad_ID;
                    Member dad = db.Members.Single(d => d.Member_ID == dadid);

                    int momid = (int)a.Mom_ID;
                    Member mom = db.Members.Single(d => d.Member_ID == momid);

                    if ((dad.Spouse_ID != mom.Member_ID && dad.Member_ID != mom.Spouse_ID) || (dad.Spouse_ID == null && mom.Spouse_ID == null))
                    {
                        a.num = dad.stt;
                        db.SaveChanges();
                    }

                    if (dad.Spouse_ID == mom.Member_ID && dad.Member_ID == mom.Spouse_ID)
                    {
                        a.num = mom.stt;
                        db.SaveChanges();
                    }
                }
                else if ((a.Dad_ID != null && a.Mom_ID == null) || (a.Dad_ID == null && a.Mom_ID != null))
                {
                    Member paren = db.Members.Single(p => p.Member_ID == id);

                    if (a.Dad_ID == id || a.Mom_ID == id)
                    {
                        a.num = paren.stt;
                        db.SaveChanges();
                    }
                }
            }
        }

        
        private TreeData.TreeDataTableDataTable GetTreeData()
        {

            List<Member> a = db.Members.Where(p => p.Parentage_ID == pID).ToList();
            TreeData.TreeDataTableDataTable dt = new TreeData.TreeDataTableDataTable();
            
            /*
            dt.AddTreeDataTableRow("1", "", "Cha", "");
            dt.AddTreeDataTableRow("2", "1", "Me", "");
            dt.AddTreeDataTableRow("3", "2", "Viet", "");
            dt.AddTreeDataTableRow("4", "2", "Vu", "");
            */

            for (int i = 0; i < a.Count; i++)
            {
                if (a[i].isWife != null && a[i].isWife == 1)
                    dt.AddTreeDataTableRow(a[i].stt.ToString(), a[i].num.ToString(), a[i].Last_Name + " " + a[i].Middle_Name + " " + a[i].First_Name, "(Spouse)");
                else
                    dt.AddTreeDataTableRow(a[i].stt.ToString(), a[i].num.ToString(), a[i].Last_Name + " " + a[i].Middle_Name + " " + a[i].First_Name, "");
            }
            
            return dt;
        }

        private void ShowTree()
        {
            pbDiagram.Image = Image.FromStream(myTree.GenerateTree(-1, -1, "1", System.Drawing.Imaging.ImageFormat.Bmp));
        }

        private void fillSTT()
        {
            List<Member> mem = db.Members.Where(m => m.Parentage_ID == pID).ToList();

            for (int i = 0; i < mem.Count; i++)
            {
                int tid = mem[i].Member_ID;
                Member tmem = db.Members.Single(m => (m.Member_ID == tid && m.Parentage_ID == pID));
                tmem.stt = i + 1;
                db.SaveChanges();
            }
        }

        private void ShowSTT()
        {
            List<Member> mem = db.Members.ToList();

            for (int i = 0; i < mem.Count; i++)
            {
                MessageBox.Show(mem[i].Last_Name + " : " + mem[i].stt.ToString() + "-" + mem[i].num.ToString());
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            ExportPicture();
        }

        private void ExportPicture()
        {
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "JPeg Image|*.jpg";
                saveFileDialog1.Title = "Save an Image File";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    pbDiagram.Image.Save(saveFileDialog1.FileName, ImageFormat.Jpeg);
                    MessageBox.Show("Successfull !", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    return;
                }

            }
            catch
            {
                MessageBox.Show("Can't export to image, please try again !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
